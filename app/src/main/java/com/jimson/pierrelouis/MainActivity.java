package com.jimson.pierrelouis;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private TextView textViewCount;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    /**pour compter le nombre de fois clique sur bouton incremente*/
    int nbfois=0;
    public void myactions(View view){
        if(view.getId()==R.id.toast){
             Toast.makeText(MainActivity.this, "vous avez clique "+nbfois +" fois", Toast.LENGTH_SHORT).show();
        }

        if(view.getId()==R.id.increment){
            /**recuperation du champs TextView*/
            textViewCount = (TextView)findViewById(R.id.textCounter);
            /**recuperation de la valeur du champs TextView*/
            int  value=Integer.parseInt(textViewCount.getText().toString());
            /**incrementation de la valeur recuperee*/
            int v=value+1;


                /**affectation de la valeur incrementee au champ TextView*/
                textViewCount.setText("" + v);
                /**incrementation de la variable nbfois qui sera affiche dans le TOAST apres click sur le bouton TOAST*/
                nbfois++;
                }
    }

}